package com.cdac.dao;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.cdac.pojos.User;


@Repository
public interface UserDao extends MongoRepository<User, String> {
}

