package com.cdac.controllers;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cdac.pojos.User;
import com.cdac.service.UserService;


@RestController
public class UserController {

	@Autowired
	private UserService userService;
	
	@GetMapping("/users")
	public List<User> getAllUsers() {
		return userService.getAllUsers();
	}
	
	@GetMapping("users/{userId}")
	public User getUserByUserId(@PathVariable String userId) {
		return userService.getUserByUserId(userId);
	}

	@PostMapping("/users")
	public String addNewUser(@RequestBody User user) {
		userService.save(user);
		return "User Added Successfully";
	}
	
	@PutMapping("/users")
	public String updateUser(@RequestBody User user) {
		userService.save(user);
		return "User Updated Successfully";
	}
	
	@DeleteMapping("/users")
	public String deleteUser(@RequestParam String userId) {
		userService.deleteUser(userId);
		return "User Deleted Successfully";
	}
}
