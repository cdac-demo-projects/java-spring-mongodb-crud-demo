package com.cdac.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;

import com.cdac.dao.UserDao;
import com.cdac.pojos.User;

@Service
public class UserService {

	@Autowired 
	private UserDao userDao;
	
	@Autowired
	private MongoTemplate mongoTemplate;
	
	public UserService() { }
	
	public List<User> getAllUsers() {
		return mongoTemplate.findAll(User.class);
	}
	
	public void save(User user) {
		userDao.save(user);
	}
	
	public User getUserByUserId(String userId) {
		Optional<User> optional = userDao.findById(userId);
		if(optional.isPresent())
			return optional.get();
		return null;
	}
	
	public void deleteUser(String userId) {
		userDao.deleteById(userId);
	}
	
}
